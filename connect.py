import subprocess
from subprocess import PIPE
import getpass
import win32gui
import win32con
import time
import PIL
import PIL.ImageGrab as ImageGrab
from pynput.keyboard import Key, Controller as KController
from pynput.mouse import Button, Controller as MController



##########################
# Constants

MASTER_PASS = ""

ACCOUNTS = ['']
PASSWORD = ""

LOGIN_COORD = (865,355)
PASSWORD_COORD = (830,420)
LOG_PASS_DIFF = PASSWORD_COORD[1] - LOGIN_COORD[1]

PLAY_POSITION = (900,580)
START_POSITION = (1126,820)

GREEN_THRESHOLD = 229 # 230
RED_THRESHOLD = 190 # 191
BLUE_THRESHOLD = 5 # 0

keyboard = KController()
mouse = MController()

CREATE_NEW_PROCESS_GROUP = 0x00000200
DETACHED_PROCESS = 0x00000008

##########################

def pressButtons(list):
    for button in list:
        keyboard.press(button)
    
    for button in list:
        keyboard.release(button)

def wait_launch(position):
    for _ in range(15):
        img = ImageGrab.grab()
        color = img.getpixel(position)
        if color[1] > GREEN_THRESHOLD and color[0] > RED_THRESHOLD and color[2] < BLUE_THRESHOLD:
            return 1
        time.sleep(1)
    return -1

def enumHandler(windows):
        for hwnd in windows:
            win32gui.ShowWindow(hwnd, win32con.SW_MAXIMIZE)
            if wait_launch(START_POSITION) == 1:
                pressButtons([Key.enter])

def main():

    for i in range(5):
        tmp = getpass.getpass("Please enter the master password : ")
        if tmp != MASTER_PASS:
            if i < 4:
                print("Wrong password, {} tries left".format(4-i))
            else:
                print("Wrong password")
                print("Exiting programm")
                exit(-1)
        else:
            print("Good password")
            break

    print("Starting dofus launcher")

    # Opens and detach dofus launcher
    subprocess.Popen('C:\\Users\\William\\AppData\\Local\\Ankama\\Dofus\\Dofus.exe', stdin=PIPE, stdout=PIPE, stderr=PIPE, creationflags=DETACHED_PROCESS | CREATE_NEW_PROCESS_GROUP)

    time.sleep(2)
    
    print("Starting {} dofus windows".format(len(ACCOUNTS)))

    pressButtons([Key.ctrl, str(len(ACCOUNTS))])

    for i,account in enumerate(ACCOUNTS):

        if i < len(ACCOUNTS) - 1 and wait_launch(PLAY_POSITION) == -1 :
            print("Account named {} is skipped as it took too much time to launch".format(ACCOUNTS[i]))
        
        else :
            #Enter login
            mouse.position = LOGIN_COORD
            mouse.click(Button.left,2)
            time.sleep(0.2)
            pressButtons([Key.delete])
            keyboard.type(account)

            time.sleep(0.5)

            #Enter password
            pressButtons([Key.tab])
            time.sleep(0.2)
            keyboard.type(PASSWORD)

            time.sleep(0.5)

            #Press enter
            pressButtons([Key.enter])

        window = win32gui.GetForegroundWindow()
        win32gui.ShowWindow(window, win32con.SW_MINIMIZE)

        time.sleep(0.5)


if __name__ == "__main__":
    main()

