# Tools Dofus
## Connect.py
Connect to all your accounts using this python script, just fill in the account names and unique password (Must all have the same pass) and optionally the master password to use on your computer.
## Dofus.ahk
Use this AutoHotKey script to switch between dofus windows instantly using your mouse buttons 5 and 6 ! Just fill in the names of character inside the Array like this Array := ["name1","name2","name3"] and the number of characters charNumer := 3 and you're good to go !
